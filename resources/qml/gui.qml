import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4

ApplicationWindow {
  id: root

  signal initNewHumanGame()
  signal initNewGameVsMonkeyAI(string monkey_color)
  signal initNewGameVsMiniMaxAI(string minimax_color)
  signal initNewGameVsMaxPieces(string maxpieces_color)
  signal initNewGameVsMaxTurn(string maxturn_color)

  signal initNewMonkeyAIGame(string monkey_one_color, string monkey_two_color)
  signal initNewMiniMonkeyAIGame(string monkey_color, string minimax_color)
  signal initNewMonkeyMaxPiecesAIGame(string monkey_color, string maxpieces_color)
  signal initNewMonkeyMaxTurnsAIGame(string monkey_color, string maxturn_color)
  signal initNewPosMaxPiecesAIGame(string minimax_color, string maxpieces_color)
  signal initNewPosMaxTurnAIGame(string minimax_color, string maxturn_color)

  signal endGameAndQuit()
  signal boardClicked(int board_pos)

  signal displayFinalScores(int player_one_score, int player_two_score)


  onDisplayFinalScores: {
    final_score_window.player_one_final_score = player_one_score
    final_score_window.player_two_final_score = player_two_score
//    final_score_window.visible = true
  }

  function endOfGameScreen(){
      if (final_score_window.player_one_final_score>final_score_window.player_two_final_score) {
          pl_two_rec.color="red"}
      else if (final_score_window.player_one_final_score<final_score_window.player_two_final_score) {
          pl_one_rec.color="red"
      }
  }

  function displayFinal(player_one_score,player_two_score, wins1, wins2, draws, games){
      final_score_window.player_one_final_score = player_one_score
      final_score_window.player_two_final_score = player_two_score
//      final_score_window.wins1 = wins1
//      final_score_window.wins2 = wins2
//      final_score_window.draws = draws
//      final_score_window.games = games
  }


  function set_bord_color (color1, color2) {
      pl_one_rec.border.color = color1
      pl_two_rec.border.color = color2
      if (color1==="black") {pl_one_rec_tex.color = "white"}
          else {pl_one_rec_tex.color = color1}
      if (color2==="white") {pl_two_rec_tex.color = "black"}
      else {pl_two_rec_tex.color = color2}
  }

  function set_game_colors (color_1, color_2, color_border_1, color_border_2) {
      gamemodel.player_one_color = color_1
      gamemodel.player_two_color = color_2
      gamemodel.player_one_border = color_border_1
      gamemodel.player_two_border = color_border_2
      pl_one_rec.color=color_1
      pl_two_rec.color=color_2
      set_bord_color (gamemodel.player_one_border,gamemodel.player_two_border)
  }

  function initNewHumanVsHuman() {
      set_game_colors("black","white","black","white")
      pl_one_rec.border.color = "lime"
      pl_two_rec.border.color = "lime"
  }



  onInitNewGameVsMonkeyAI: {
      set_game_colors("black","white","black",monkey_color)
      pl_one_rec.border.color = "lime"
  }

  onInitNewGameVsMiniMaxAI: {
      set_game_colors("black","white","black",minimax_color)
      pl_one_rec.border.color = "lime"
  }

  onInitNewGameVsMaxPieces: {
      set_game_colors("black","white","black",maxpieces_color)
      pl_one_rec.border.color = "lime"
  }

  onInitNewGameVsMaxTurn: {
      set_game_colors("black","white","black",maxturn_color)
      pl_one_rec.border.color = "lime"
  }

  onInitNewMonkeyAIGame: {set_game_colors("black","white",monkey_one_color,monkey_two_color)
  }

  onInitNewMiniMonkeyAIGame: {
      set_game_colors("black","white",monkey_color,minimax_color)
  }

  onInitNewMonkeyMaxPiecesAIGame: {
      set_game_colors("black","white",monkey_color,maxpieces_color)
  }

  onInitNewMonkeyMaxTurnsAIGame: {
      set_game_colors("black","white",monkey_color,maxturn_color)
  }

  onInitNewPosMaxTurnAIGame: {
      set_game_colors("black","white",minimax_color,maxturn_color)
  }

  onInitNewPosMaxPiecesAIGame: {
      set_game_colors("black","white",minimax_color,maxpieces_color)
  }


  function initChangeColor() {
    gamemodel.player_one_color = "red"
    gamemodel.player_two_color = "green"
  }

  visible: true

  title: qsTr("Othello Game")

//    onHeightChanged: { width=1.2*height
//    }

//    onWidthChanged: {
//        root.height = 0.8*root.width
//    }

  minimumWidth : 1000
  minimumHeight : 800
  width : 1000
  height : 800
  maximumHeight: 800
  maximumWidth: 1000

  color: "grey"

  menuBar: MenuBar {
        Menu {
          title: "Human Games"
          MenuItem {
            text: "Human VS Human"
            shortcut: "Ctrl+N"
            onTriggered: initNewHumanGame()
          }
          MenuItem {
            text: "Human VS Monkey"
            shortcut: "Ctrl+M"
            onTriggered: initNewGameVsMonkeyAI("orange")
          }
          MenuItem {
            text: "Human VS Positional"
            shortcut: "Ctrl+P"
            onTriggered: initNewGameVsMiniMaxAI("brown")
          }
          MenuItem {
            text: "Human VS MaxPieces"
            shortcut: "Ctrl+B"
            onTriggered: initNewGameVsMaxPieces("blue")
          }
          MenuItem {
            text: "Human VS MaxTurn"
            shortcut: "Ctrl+T"
            onTriggered: initNewGameVsMaxTurn("magenta")
          }
}
        Menu {
            title: "Simulations"
            MenuItem {
              text: "Monkey VS Monkey"
              onTriggered: initNewMonkeyAIGame("orange","orange")
            }
            MenuItem {
              text: "Monkey VS Positional"
              onTriggered: initNewMiniMonkeyAIGame("orange","brown")
            }
            MenuItem {
              text: "Monkey VS MaxPieces"
              onTriggered: initNewMonkeyMaxPiecesAIGame("orange","blue")
            }
            MenuItem {
                text: "Monkey VS MaxTurn"
                onTriggered: initNewMonkeyMaxTurnsAIGame("orange","magenta")
            }
            MenuItem {
              text: "Positional VS MaxPieces"
              onTriggered: initNewPosMaxPiecesAIGame("brown","blue")
            }
            MenuItem {
              text: "Positional VS MaxTurn"
              onTriggered: initNewPosMaxTurnAIGame("brown","magenta")
            }
        }
        Menu{
            title: "Quit"
            MenuItem {
                text: "Quit"
                shortcut: "Ctrl+Q"
                onTriggered: endGameAndQuit()
            }
        }
    }




Rectangle {
    id: main_rec
    width:Math.min(Math.min(parent.width, parent.height),parent.width*0.8)
    height:width

  GridLayout {
    anchors.fill: parent
    columns: 8
    rows: 8
    columnSpacing: 0.2
    rowSpacing: 0.2

    Repeater {
      model: VisualDataModel {
        model: gamemodel
        delegate: Rectangle {

            id: piece_rec
            property bool contains_mouse: false
            property string board_color: "green"

            Layout.fillHeight: true
            Layout.fillWidth: true

            color : "green"

            Rectangle {
              anchors.centerIn: parent
              width: Math.min(parent.width, parent.height) * 0.7
              height: width
              radius: width * 0.5

              color: {
                if(occupied)
                  return playernr === 0 ? gamemodel.player_one_color : gamemodel.player_two_color
                else if(!occupied && legalmoves)
                    if (!contains_mouse) return "darkgreen"
                    else return gamemodel.currentPlayer === 0 ? gamemodel.player_one_color : gamemodel.player_two_color
                else if(!occupied && contains_mouse)
                  return gamemodel.currentPlayer === 0 ? gamemodel.player_one_color : gamemodel.player_two_color
                else return board_color
              }

              border.color : {
                  if(occupied)
                    return playernr === 0 ? gamemodel.player_one_border : gamemodel.player_two_border
                  else if(!occupied && contains_mouse)
                    return gamemodel.currentPlayer === 0 ? gamemodel.player_one_border : gamemodel.player_two_border
                  else return board_color
              }

//              border.width: { if (gamemodel.currentPlayer!==playernr) return 2 ; else return 0; }
              border.width : 2

            }

            border.color: "black"
            border.width: 1
            MouseArea {
              anchors.fill: parent
              onClicked: {
                boardClicked(piecenr)
              }

              hoverEnabled: true
              onContainsMouseChanged: piece_rec.contains_mouse = containsMouse

            }
        }
      }
    }
  }
}


  Rectangle {
      width:parent.width-main_rec.width;
      height:parent.height;

    id: final_score_window
    visible: true

    property int player_one_final_score: 0
    property int player_two_final_score: 0
    property int wins1: 0
    property int wins2: 0
    property int draws: 0
    property int games: 0

    anchors.right: parent.right

    Rectangle {
              height:parent.height/2
              width:parent.width
              id: pl_one_rec
              border.width : { if (gamemodel.currentPlayer ===0) return 4 ; else return 0; }
              color: "black"
              Text{ id:pl_one_rec_tex; font.pointSize: 13; font.bold: true; anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter;
                    text: "Player one: " + final_score_window.player_one_final_score}
          }
    Rectangle {
        anchors.top: pl_one_rec.bottom
              height:parent.height/2
              width:parent.width
              id: pl_two_rec
              border.width : { if (gamemodel.currentPlayer ===1) return 4 ; else return 0; }
              color: "white"
              Text{ id:pl_two_rec_tex; font.pointSize: 13; font.bold: true; anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter;
                    text: "Player two: " + final_score_window.player_two_final_score}
      }
  }
}
