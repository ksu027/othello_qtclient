//template for testing next position

int positest = 63;
qDebug() << "next position N " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::N).value();
qDebug() << "next position NW " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::NW).value();
qDebug() << "next position W " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::W).value();
qDebug() << "next position SW " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::SW).value();
qDebug() << "next position S " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::S).value();
qDebug() << "next position SE " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::SE).value();
qDebug() << "next position E " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::E).value();
qDebug() << "next position NE " << othello::utility::nextPosition(othello::BitPos(positest),othello::MoveDirection::NE).value();


//template for testing bracketing points

/* testing of bracketing piece */
//othello::BitPos testPos;
othello::BitBoard testBoard;
//othello::PlayerId testPlayerID;
//othello::MoveDirection testMove;

testBoard[0].reset();
testBoard[1].reset();

testBoard[0].set(9);
testBoard[0].set(13);
testBoard[0].set(21);
testBoard[0].set(53);

testBoard[1].set(17);
testBoard[1].set(37);
testBoard[1].set(40);
testBoard[1].set(45);
testBoard[1].set(51);
testBoard[1].set(59);

//testPos=othello::BitPos(25);
//testMove = othello::MoveDirection::S;
//testPlayerID = othello::PlayerId::One;

qDebug() << "Bracketing Piece:" << othello::utility::findBracketingPiece(testBoard,othello::BitPos(53),othello::PlayerId::One,othello::MoveDirection::S).value();







othello::BitPos fortest;

othello::BitBoard testBoard;
othello::BitPosSet testBitPos;

testBoard[0].reset();
testBoard[1].reset();

testBoard[0].set(28);
testBoard[0].set(35);

testBoard[1].set(27);
testBoard[1].set(36);

/* add checking of empty result bracketingPieces*/
testBitPos=othello::utility::legalMoves(testBoard, othello::PlayerId::Two);

if (testBitPos.size()>0) {
for (auto i=testBitPos.begin(); i != testBitPos.end() ; ++i) {
    fortest = *i;
    qDebug() <<fortest.value()<<"\n";
}
};

for (int j=0; j<64 ; j++) {
 if (othello::utility::isLegalMove(testBoard, othello::PlayerId::One,othello::BitPos(j))) {qDebug() << j << " is legal for player One \n";};
 };

for (int j=0; j<64 ; j++) {
 if (othello::utility::isLegalMove(testBoard, othello::PlayerId::Two,othello::BitPos(j))) {qDebug() << j << " is legal for player Two \n";};
 };
