#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// local
#include "gamemodel.h"

// othello library
#include <engine.h>
#include <minimax_ai.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSizeF>
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrent>

// stl
#include <memory>


class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  GuiApplication(int& argc, char** argv);
  ~GuiApplication() override = default;

private:
//  othello::OthelloGameEngine m_game_engine;
  std::array<othello::OthelloGameEngine,1> m_game_engine;

  QFutureWatcher<void> m_watcher;

  GameModel m_model;
  QQmlApplicationEngine m_app;
  void turnEnded();
  void treeOut( othello::minimax_ais::MiniMaxAI::Node <othello::minimax_ais::MiniMaxAI::MiniMaxData>& parent,size_t level);
  int wins1,wins2,draws,games;

private slots:
  void initNewHumanGame();
  void initNewGameVsMonkeyAI();
  void initNewMonkeyAIGame();
  void initNewGameVsMiniMaxAI();
  void initNewMiniMonkeyAIGame();


  void initNewGameVsMaxPieces();
  void initNewGameVsMaxTurn();

  void initNewMonkeyMaxPiecesAIGame();
  void initNewMonkeyMaxTurnsAIGame();
  void initNewPosMaxPiecesAIGame();
  void initNewPosMaxTurnAIGame();

  void endGameAndQuit();
  void endOfGameActions();
  void startNextTurn();
  void boardClicked(int);

signals:
  void initNewHumanGameSignal();
  void enqueueNextTurn();
  void gameEnded();
  void displayFinalScores(int player_one_score, int player_two_score);
  void displayFinal(QVariant player_one_score, QVariant player_two_score, QVariant wins1, QVariant wins2,QVariant draws,QVariant games);
  void endOfGameScreen();

};   // END class GuiApplication

#endif   // GUIAPPLICATION_H
