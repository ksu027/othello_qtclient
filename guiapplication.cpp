#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>

// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>

// stl
#include <chrono>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv),
    m_game_engine{}, m_model{m_game_engine.at(0)}, m_app{}
{

  m_app.rootContext()->setContextProperty("gamemodel", &m_model);

  m_app.load(QUrl("qrc:/qml/gui.qml"));

  auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
  if (root_window) {

    connect(root_window, SIGNAL(initNewHumanGame()), this,
            SLOT(initNewHumanGame()));

    connect(root_window, SIGNAL(initNewGameVsMonkeyAI(QString)), this,
            SLOT(initNewGameVsMonkeyAI()));

    connect(root_window, SIGNAL(initNewGameVsMiniMaxAI(QString)), this,
            SLOT(initNewGameVsMiniMaxAI()));



    connect(root_window, SIGNAL(initNewGameVsMaxPieces(QString)), this,
            SLOT(initNewGameVsMaxPieces()));

    connect(root_window, SIGNAL(initNewGameVsMaxTurn(QString)), this,
            SLOT(initNewGameVsMaxTurn()));



    connect(root_window, SIGNAL(initNewMonkeyAIGame(QString,QString)), this,
            SLOT(initNewMonkeyAIGame()));

    connect(root_window, SIGNAL(initNewMiniMonkeyAIGame(QString,QString)), this,
            SLOT(initNewMiniMonkeyAIGame()));



    connect(root_window, SIGNAL(initNewMonkeyMaxPiecesAIGame(QString,QString)), this,
            SLOT(initNewMonkeyMaxPiecesAIGame()));

    connect(root_window, SIGNAL(initNewMonkeyMaxTurnsAIGame(QString,QString)), this,
            SLOT(initNewMonkeyMaxTurnsAIGame()));

    connect(root_window, SIGNAL(initNewPosMaxPiecesAIGame(QString,QString)), this,
            SLOT(initNewPosMaxPiecesAIGame()));

    connect(root_window, SIGNAL(initNewPosMaxTurnAIGame(QString,QString)), this,
            SLOT(initNewPosMaxTurnAIGame()));


    connect(root_window, SIGNAL(endGameAndQuit()), this,
            SLOT(endGameAndQuit()));

    connect(this, &GuiApplication::gameEnded, this,
            &GuiApplication::endOfGameActions);

    connect(root_window, SIGNAL(boardClicked(int)), this,
            SLOT(boardClicked(int)));

    connect(this, SIGNAL(endOfGameScreen()), root_window,
            SLOT(endOfGameScreen()));

    connect(this, SIGNAL(displayFinal(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)), root_window,
            SLOT(displayFinal(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)));

    connect(this, SIGNAL(initNewHumanGameSignal()), root_window,
            SLOT(initNewHumanVsHuman()));

    connect( &m_watcher, &QFutureWatcher<void>::finished, this, &GuiApplication::turnEnded );

  }

  wins1=wins2=draws=games=0;
  //starting screen

  initNewHumanGame();


}

void GuiApplication::startNextTurn()
{
    /* startNextTurn wothout loop */
    if (m_game_engine.at(0).endOfGame()) {gameEnded();}
    else {
        qDebug() <<"CurPlayer "<<size_t(m_game_engine.at(0).currentPlayerId());
        m_game_engine.at(0).changePlayer();

        m_model.update();

        if (m_game_engine.at(0).passTurn())
        {
            qDebug() <<"Player "<<size_t(m_game_engine.at(0).currentPlayerId())<<" passes the turn";
            m_game_engine.at(0).changePlayer();
        }

        if (m_game_engine.at(0).currentPlayerType()==othello::PlayerType::AI) {
            QFuture<void> future  = QtConcurrent::map( m_game_engine, [](auto& engine) { return engine.think(2s); });
            m_watcher.setFuture(future);
        }
    }
    return;
}

void GuiApplication::boardClicked(int board_pos)
{
    if (m_game_engine.at(0).performMoveForCurrentHuman(othello::BitPos(board_pos)))
    {
        turnEnded();
    } else {
        qDebug()<<"That is incorrect position";
    };

}

void GuiApplication::initNewHumanGame()
{
   m_game_engine.at(0).initNewGame();
   initNewHumanGameSignal();
   /* initialize players and set current player*/
   m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
   m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::Two>();

   turnEnded();
}

void GuiApplication::initNewGameVsMonkeyAI()
{
   m_game_engine.at(0).initNewGame();
   /* initialize players and set current player*/
   m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
   m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::Two>();
   turnEnded();
}

void GuiApplication::initNewMonkeyAIGame()
{
    m_game_engine.at(0).initNewGame();
    /* initialize players and set current player*/
    m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::Two>();
    turnEnded();
}

void GuiApplication::initNewGameVsMiniMaxAI()
{
    m_game_engine.at(0).initNewGame();
    /* initialize players and set current player*/
    m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxAI,othello::PlayerId::Two>();
    turnEnded();
}

void GuiApplication::initNewMiniMonkeyAIGame()
{
    m_game_engine.at(0).initNewGame();
    /* initialize players and set current player*/

    //m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxAI,othello::PlayerId::Two>();
    turnEnded();
}



void GuiApplication::endGameAndQuit() { QGuiApplication::quit(); }


void GuiApplication::endOfGameActions()
{   
    /*temp for statistics*/
    games+=1;
    if (m_game_engine.at(0).score_player_one > m_game_engine.at(0).score_player_two) wins1+=1;
    else if (m_game_engine.at(0).score_player_one < m_game_engine.at(0).score_player_two) wins2+=1;
    else draws+=1;
    /*temp*/
    //displayFinal(m_game_engine.at(0).score_player_one, m_game_engine.at(0).score_player_two, wins1,wins2,draws, games);



    endOfGameScreen();
    m_model.update();

//    if (games<100) initNewMiniMonkeyAIGame();

}

void GuiApplication::turnEnded() {
    m_game_engine.at(0).countScore(m_game_engine.at(0).currentPlayerId());
    displayFinal(m_game_engine.at(0).score_player_one, m_game_engine.at(0).score_player_two, wins1,wins2,draws, games);
//    m_model.update();
    startNextTurn();
}

void GuiApplication::treeOut( othello::minimax_ais::MiniMaxAI::Node <othello::minimax_ais::MiniMaxAI::MiniMaxData>& parent,size_t level)
{
    if (level==0) { return; }
    qDebug()<<"____"<<level;
    for (auto& tree_node : parent.child ) {
        qDebug()<<level<<" level "<<tree_node.data.costf;
        treeOut(tree_node,level-1);
    }
}

/* temporary like that */
void GuiApplication::initNewGameVsMaxPieces(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxPieces,othello::PlayerId::Two>();
    turnEnded();
};
void GuiApplication::initNewGameVsMaxTurn(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxTurn,othello::PlayerId::Two>();
    turnEnded();
};

void GuiApplication::initNewMonkeyMaxPiecesAIGame(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxPieces,othello::PlayerId::Two>();
    turnEnded();
};
void GuiApplication::initNewMonkeyMaxTurnsAIGame(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxTurn,othello::PlayerId::Two>();
    turnEnded();
};
void GuiApplication::initNewPosMaxPiecesAIGame(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxPieces,othello::PlayerId::Two>();
    turnEnded();
};
void GuiApplication::initNewPosMaxTurnAIGame(){
    m_game_engine.at(0).initNewGame();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxAI,othello::PlayerId::One>();
    m_game_engine.at(0).initPlayerType<othello::minimax_ais::MiniMaxTurn,othello::PlayerId::Two>();
    turnEnded();
};
